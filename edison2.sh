#!/bin/zsh -l

#SBATCH -N 1
#SBATCH -t 00:30:00
#SBATCH --qos=debug

cd /global/homes/j/jemeyers/src/sky_bg_benchmark_edison/
srun -n 1 -c 48 -J sky_bg_benchmark_edison time python sky_background.py --npix=100
