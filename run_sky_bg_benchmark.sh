#!/bin/bash

# This takes ~700s on Cori-Haswell and scales as npix**2
time python sky_background.py --npix=1000
