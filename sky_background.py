import sys
import numpy as np
import galsim

def timer(f):
    import functools

    @functools.wraps(f)
    def f2(*args, **kwargs):
        import time
        t0 = time.time()
        result = f(*args, **kwargs)
        t1 = time.time()
        fname = repr(f).split()[1]
        print('time for %s = %.2f' % (fname, t1-t0))
        return result
    return f2

#@timer
def get_nphotons(image, skyCounts, rng):
    npix = np.prod(image.array.shape)
    # Return the total number of sky bg photons drawn from a
    # Poisson distribution.
    return int(galsim.PoissonDeviate(rng, mean=npix*skyCounts)())

#@timer
def get_photon_array(image, nphotons, rng):
    """
    Generate an array of photons randomly distributed over the
    surface of the sensor.
    """
    photon_array = galsim.PhotonArray(int(nphotons))

    # Generate the x,y values.
    rng.generate(photon_array.x) # 0..1 so far
    photon_array.x *= (image.xmax - image.xmin + 1)
    photon_array.x += image.xmin - 0.5  # Now from xmin-0.5 .. xmax+0.5
    rng.generate(photon_array.y)
    photon_array.y *= (image.ymax - image.ymin + 1)
    photon_array.y += image.ymin - 0.5
    photon_array.flux = 1

    return photon_array

@timer
def get_waves(rng, sed_file, bp_file):
    sky_sed = np.recfromtxt(sed_file, names='x f'.split())
    sed = galsim.SED(galsim.LookupTable(x=sky_sed.x, f=sky_sed.f),
                     wave_type='nm', flux_type='flambda')
    bp = np.recfromtxt(bp_file, names='wl sb'.split())
    index = np.where(bp.sb != 0)
    bandpass \
        = galsim.Bandpass(galsim.LookupTable(x=bp.wl[index], f=bp.sb[index]),
                          wave_type='nm')
    return galsim.WavelengthSampler(sed=sed, bandpass=bandpass, rng=rng)

@timer
def get_angles(rng):
    fratio = 1.234  # From https://www.lsst.org/scientists/keynumbers
    obscuration = 0.606  # (8.4**2 - 6.68**2)**0.5 / 8.4
    return galsim.FRatioAngles(fratio, obscuration, rng)

@timer
def sky_background(sky_level=1000, nrow=400, ncol=400, seed=194019,
                   chunk_size=int(5e6), sed_file='sky_sed.txt',
                   bp_file='bandpass_i.txt'):
    treering_func = galsim.SiliconSensor.simple_treerings(0.26, 47.)
    treering_center = galsim.PositionD(0, 0)
    rng = galsim.UniformDeviate(seed)

    image = galsim.ImageF(ncol, nrow)
    nrecalc = 1e300
    bf_strength = 1.

    waves = get_waves(rng, sed_file, bp_file)
    angles = get_angles(rng)

    nx, ny = 2, 8
    dx = ncol//nx
    dy = nrow//ny
    sensor = galsim.SiliconSensor(rng=rng, nrecalc=nrecalc,
                                  strength=bf_strength,
                                  treering_func=treering_func,
                                  treering_center=treering_center)
    total_photons = 0
    for i in range(nx):
        xmin = i*dx + 1
        xmax = (i + 1)*dx
        for j in range(ny):
            sys.stdout.write('{} {}, '.format(i, j))
            sys.stdout.flush()
            ymin = j*dy + 1
            ymax = (j + 1)*dy
            amp_bounds = galsim.BoundsI(xmin, xmax, ymin, ymax)
            # Create a temporary image to contain the single amp data
            # with a 1-pixel buffer around the perimeter.
            temp_image = galsim.ImageF(ncol, nrow)
            bounds = (galsim.BoundsI(xmin-1, xmax+1, ymin-1, ymax+1)
                      & temp_image.bounds)
            temp_amp = temp_image[bounds]
            nphotons = get_nphotons(temp_amp, sky_level, rng)
            total_photons += nphotons
            chunks = [chunk_size]*(nphotons//chunk_size)
            if nphotons % chunk_size > 0:
                chunks.append(nphotons % chunk_size)

            for ichunk, nphot in enumerate(chunks):
                photon_array = get_photon_array(temp_amp, nphot, rng)
                waves.applyTo(photon_array)
                angles.applyTo(photon_array)

                # Accumulate the photons on the temporary amp image.
                sensor.accumulate(photon_array, temp_amp, resume=(ichunk > 0))

            # Add the temp_amp image to the final image, excluding
            # the 1-pixel buffer.
            image[amp_bounds] += temp_image[amp_bounds]
    print()
    print("total photons = {:_}".format(total_photons))
    return image

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--sky_level', type=float, default=1000.,
                        help='Sky level in photons/pixel')
    parser.add_argument('--npix', type=int, default=80,
                        help='Number of pixels in x- and y-directions.')
    parser.add_argument('--seed', type=int, default=None,
                        help='Random seed.')
    parser.add_argument('--chunk_size', type=int, default=int(5e6),
                        help='Size of photon arrays to process in '
                        'each iteration')
    parser.add_argument('--sed_file', type=str, default='sky_sed.txt',
                        help='Sky SED file')
    parser.add_argument('--bp_file', type=str, default='bandpass_i.txt',
                        help='band pass file')
    parser.add_argument('--outfile', type=str, default=None,
                        help='File name for output FITS file')

    args = parser.parse_args()
    print(args)
    image = sky_background(sky_level=args.sky_level,
                           nrow=args.npix,
                           ncol=args.npix,
                           seed=args.seed,
                           chunk_size=args.chunk_size,
                           sed_file=args.sed_file,
                           bp_file=args.bp_file)

    if args.outfile is not None:
        image.write(args.outfile)
