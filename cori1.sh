#!/bin/zsh -l

#SBATCH --qos=debug
#SBATCH -N 1
#SBATCH -t 00:30:00
#SBATCH -C haswell

cd /global/homes/j/jemeyers/src/sky_bg_benchmark_cori/
srun -n 1 -c 64 -J sky_bg_benchmark_cori time python sky_background.py --npix=30
