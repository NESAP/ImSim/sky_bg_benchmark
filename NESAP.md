See README.md for instructions to setup and run the benchmark.
Job submission scripts for Edison and Cori Haswell are included
in the repo.

Results:

npix | Edison time | Cori Haswell time
-----|-------------|------------------
30   | 1.06 s      | 1.24 s
100  | 7.76 s      | 6.56 s
300  | 69.39 s     | 60.23 s
1000 | 781.86 s    | 679.54 s
