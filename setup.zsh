export STACKCVMFS=/cvmfs/sw.lsst.eu/linux-x86_64
export LSST_STACK_VERSION=w_2019_15

module unload python
module swap PrgEnv-intel PrgEnv-gnu
module load pe_archive
module swap gcc gcc/6.3.0
module rm craype-network-aries
module rm cray-libsci
module unload craype
export CC=gcc

# Use the /cvmfs distrubtion from IN2P3
LSST_DISTRIB=$STACKCVMFS/lsst_distrib/$LSST_STACK_VERSION
LSST_SIMS=$STACKCVMFS/lsst_sims/sims_$LSST_STACK_VERSION

EUPS_DIR="${LSST_DISTRIB}/eups/current"
source "${LSST_DISTRIB}/loadLSST.zsh"

# Tell eups to also use the packages in lsst_sims on top of lsst_distrib
EUPS_PATH=$EUPS_PATH:"${LSST_SIMS}/stack/current"
setup lsst_sims
