# sky_bg_benchmark

Benchmark code for sky background rendering using galsim.SiliconSensor

To run at NERSC from bash:
```
$ source ./setup.sh
$ ./run_sky_bg_benchmark.sh
```
